
package modelo;
import java.sql.*;

/**
 *
 * @author patri
 */
public abstract class dbManejador {
    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registros;
    private String usuario,contaseña,baseDatos,drive, url;

    public dbManejador(Connection conexion, PreparedStatement sqlConsulta,
            ResultSet registros, String usuario, String contaseña, String baseDatos, String drive, String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registros = registros;
        this.usuario = usuario;
        this.contaseña = contaseña;
        this.baseDatos = baseDatos;
        this.drive = drive;
        this.url = url;
    }
    
      public dbManejador(){
      
      this.baseDatos="sistemas";
      this.usuario="root";
      this.contaseña="123";
      this.drive = "com.mysql.cj.jdbc.Driver";
      this.url ="jdbc:mysql://localhost:3306/" +this.baseDatos;
      this.EsDrive();
      }

      public boolean EsDrive(){
      boolean exito = false;
       
      try {
          Class.forName(drive);
          exito =true;
          
      } catch(ClassNotFoundException e){
      
          System.out.println("Surgio un error " + e.getMessage());
          System.exit(-1);
      }
      
     return exito; 
      
      }
      
      
      
      public boolean conectar(){
      boolean exito = false;
      try {
      
      this.setConexion(DriverManager.getConnection(url, usuario, this.contaseña));
      exito = true;
      
      } catch(SQLException e){
          System.out.println("Surgio un error " + e.getMessage());
      
      }
       return exito;
      }
      
      public void desconectar(){
           try{
               // validar que conexion esta abierta 
            if(!this.conexion.isClosed()) this.conexion.close();
           
           } catch(SQLException e){
               
               System.out.println("Surgio un error al cerrar conexion " +e.getMessage());
           
           }
      }
      
      
    public Connection getConexion() {
        return conexion;
    }
    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }
    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }
    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }
    public ResultSet getRegistros() {
        return registros;
    }
    public void setRegistros(ResultSet registros) {
        this.registros = registros;
    }
    public String getUsuario() {
        return usuario;
    }
    
    public String getContaseña() {
        return contaseña;
    }
    public void setContaseña(String contaseña) {
        this.contaseña = contaseña;
    }

    public String getBaseDatos() {
        return baseDatos;
    }
    public void setBaseDatos(String baseDatos) {
        this.baseDatos = baseDatos;
    }
    public String getDrive() {
        return drive;
    }
    public void setDrive(String drive) {
        this.drive = drive;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
      
    
}
